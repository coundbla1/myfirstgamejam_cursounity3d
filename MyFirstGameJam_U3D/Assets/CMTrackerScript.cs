﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMTrackerScript : MonoBehaviour
{
    public GameObject Champ;    
    private Rigidbody2D rigidbody2D;
    private ChampionScript championScript;
    public float TrackerSpeed=0;
    // Start is called before the first frame update
    void Start()
    {
        transform.position = Champ.transform.position;
        rigidbody2D = GetComponent<Rigidbody2D>();
        championScript = Champ.GetComponent<ChampionScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Champ.transform.position.y >= -15 && Champ.transform.position.y <= 24)
            transform.position = new Vector2(transform.position.x, Champ.transform.position.y);
    }
    private void FixedUpdate()
    {
        if (!championScript.StartGameFl)
        {
            rigidbody2D.velocity = new Vector2(0,0);
        }
        else
        {
            if (Champ.transform.position.x <= transform.position.x-2f)
                rigidbody2D.velocity = new Vector2((championScript.Speed / 4 + TrackerSpeed), rigidbody2D.velocity.y);
            else
                rigidbody2D.velocity = new Vector2(championScript.Speed + TrackerSpeed, rigidbody2D.velocity.y);
        }        
    }
    public void RaiseTrackerSpeed()
    {

    }
}
