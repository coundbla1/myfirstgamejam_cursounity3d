using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartMap startMap = transform.parent.gameObject.GetComponent<StartMap>();
        CameraScript cameraScript = Camera.main.GetComponent<CameraScript>();
        if (!startMap)
        {
            Debug.Log("ERROR AL INICIAR MAPA");
        }
        if (!cameraScript)
        {
            Debug.Log("ERROR AL INICIAR CAMARA");
        }
        startMap.StartBG();
        cameraScript.StartCamera();
    }
}
