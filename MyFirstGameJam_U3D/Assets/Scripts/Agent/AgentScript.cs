using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;

public class AgentScript : Agent
{
    
    [SerializeField] private Transform cameralimitTramsform;
    [SerializeField] private Transform objetiveTransform;
    [SerializeField] private float jumpTime = 0.25f;
    [SerializeField] private ChampionScript champScript;
    [SerializeField] private CameraScript cameraScript;
    [SerializeField] private float lastjump;
    [SerializeField] private float time;
    [SerializeField] private GameObject MaxScoreInfo;

    private Vector3 GetCameraRP()
    {
        Vector3 cameraRelativePosition = new Vector3(-200,0) - transform.position;

        if(cameralimitTramsform == null)
        {
            cameralimitTramsform = cameraScript.GetCameraLimit();
        }
        if(cameralimitTramsform != null)
        {
            cameraRelativePosition = transform.position - cameralimitTramsform.position;
        }

        return cameraRelativePosition;
    }

    private Vector3 GetObjetive()
    {
        Vector3 objetiveRelativePosition = new Vector3(1000-transform.position.x,0);

        if(objetiveTransform == null)
        {
            StartMap startmap = GameObject.Find("MapGeneratorController").GetComponent<StartMap>();
            GameObject block = startmap.lastBlockInserted;
            if(block != null)
            {
                objetiveTransform = block.transform.Find("SpawnNextTileMap");
                if (objetiveTransform != null)
                {
                    objetiveRelativePosition = objetiveTransform.position - transform.position;
                }
            }            
        }
        else
        {
            objetiveRelativePosition = objetiveTransform.position - transform.position;
        }
        return objetiveRelativePosition;
    }
    
    public override void OnEpisodeBegin()
    {
        Debug.Log("EPISODE BEGIN");
        champScript = transform.GetComponent<ChampionScript>();
        cameraScript = Camera.main.GetComponent<CameraScript>();
        cameralimitTramsform = cameraScript.GetCameraLimit();
        time = 0;
        //A�adimos puntuaci�n m�xima obtenida por el agente
        int MaxScore = PlayerPrefs.GetInt("MaxScore", 0);
        TextMeshProUGUI textMeshPro = MaxScoreInfo.GetComponent<TextMeshProUGUI>();
        textMeshPro.text = MaxScore.ToString();

    }

    public override void CollectObservations(VectorSensor sensor)
    {
        time += Time.deltaTime;

        sensor.AddObservation(transform.position); //-> vector3
        //Sensor que indica si el personaje est� en el suelo
        sensor.AddObservation(champScript.getGrounded());
        //posici�n relativa al collider de la camara -> vector3
        sensor.AddObservation(GetCameraRP().normalized);
        //posici�n relativa al spawner de mapa. ->vector3 -> ocupa 3 espacios
        sensor.AddObservation(GetObjetive().normalized);
        
        //datos del champ
        sensor.AddObservation(champScript.JumpForce);
        sensor.AddObservation(champScript.Speed);
        sensor.AddObservation(champScript.Level);
        sensor.AddObservation(champScript.Exp);

        //sensores que a�aden m�s visi�n al agente

        sensor.AddObservation(champScript.getFrontObstacle());
        sensor.AddObservation(champScript.getPosibleJumpDistance());
        sensor.AddObservation(champScript.getTopWay());                
    }

    public override void OnActionReceived(ActionBuffers actions)
    {
        float moveX = actions.ContinuousActions[0];
        int jump = actions.DiscreteActions[0];

        champScript.setHorizontal(moveX);
        if(jump == 1)
        {
            Debug.Log(GetCumulativeReward());
            Debug.Log("DISCRETEACTIONS: " + jump);
            //if(lastjump+jumpTime <= time && champScript.getGrounded())
            //{
                champScript.JumpController();
                lastjump = time;
            //}            
        }        
    }
    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> contnuosActions = actionsOut.ContinuousActions;
        ActionSegment<int> discreteActions = actionsOut.DiscreteActions;

        contnuosActions[0] = Input.GetAxisRaw("Horizontal");
        discreteActions[0] = Input.GetKey(KeyCode.Space) ? 1:0;
        Debug.Log(GetCumulativeReward());
        Debug.Log("DISCRETEACTIONS: " + discreteActions[0]);
        if(discreteActions[0] == 1)
        {
            Debug.Log(GetCumulativeReward());
            Debug.Log("DISCRETEACTIONS: " + discreteActions[0]);
            if (lastjump + jumpTime <= time)
            {
                champScript.JumpController();
                lastjump = time;
            }
        }
 
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        CameraScript camScript = collision.GetComponent<CameraScript>();
        if (camScript != null)
        {
            EndAgentEpisode(0);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        CameraScript camScript = collision.collider.GetComponent<CameraScript>();
        if (camScript != null && champScript.getWallCollide())
        {
            EndAgentEpisode(0);
        }
    }
    public void EndAgentEpisode(float reward)
    {
        SetReward(reward);
        champScript.Kill();
        champScript.RestartGame();          
    }
    public void setChampScript(ChampionScript champ)
    {
        champScript = champ;
    }

    public void SetCameraLimit(Transform transform)
    {
        cameralimitTramsform = transform;
    }

    public void setObjetive(Transform objetive)
    {
        objetiveTransform = objetive;
    }
}
