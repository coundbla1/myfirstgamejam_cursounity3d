﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpCristalScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        ChampionScript champ = collision.GetComponent<ChampionScript>();        

        if (champ != null)
        {                       
            champ.AddExp(2);
            champ.AddOtherScore(5);
            Destroy(gameObject);
        }
    }
}
