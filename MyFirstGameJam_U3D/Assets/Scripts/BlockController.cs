﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour
{
    public List<GameObject> listObjetos = new List<GameObject>();
    public int numObjetos = 0;
    public void UpdateArrayObjects(GameObject obj)
    {
        listObjetos.Add(obj);
        numObjetos++;
    }
    public void OnDestroy()
    {
        foreach(GameObject objeto in listObjetos)
        {
            Destroy(objeto);
        }
    }
}
