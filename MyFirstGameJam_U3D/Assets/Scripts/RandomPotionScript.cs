﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPotionScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        ChampionScript champ = collision.GetComponent<ChampionScript>();
        
        if (champ != null)
        {
            float rNumber = Random.Range(0, 3);
            if (rNumber < 1)//JumpPotion
            {
                champ.EditJumpForce(150f);
            }
            else
            {
                if(rNumber < 1)//SpeedPotion
                {
                    champ.EditSpeed(1f);
                }
                else if (rNumber < 2)//MoreWeightPotion
                {
                    champ.AddWeight(0.5f);
                }
                else if (rNumber < 3)//SlowPotion
                {
                    champ.EditSpeed(-0.5f);
                }
            }
            champ.AddExp(15);
            champ.AddOtherScore(30);
            Destroy(gameObject);
        }
    }
}
