﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZoneScript : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        ChampionScript champ =  collision.collider.GetComponent<ChampionScript>();

        if(champ != null)
        {
            if (!champ.IsAgentFl)
            {
                champ.Kill();
            }
            else
            {
                AgentScript agent = champ.transform.GetComponent<AgentScript>();
                agent.EndAgentEpisode(0);
            }
        }
    }
}
