﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPotionScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        ChampionScript champ = collision.GetComponent<ChampionScript>();

        if (champ != null)
        {
            /*Version antigua*/
            //champ.EditJumpForce(150f);
            champ.EditJumpForce(50f);
            champ.AddExp(10);
            champ.AddOtherScore(20);
            Destroy(gameObject);
        }
    }
}
