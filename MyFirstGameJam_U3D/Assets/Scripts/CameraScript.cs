﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*Script que controla el movimiento de la camara*/
public class CameraScript : MonoBehaviour
{
    //variables públicas 
    public GameObject Champ;
    public StartMap startMap;
    public bool onMoveFl;
    public float cameraSpeed = 0.01f;
    public int blocksToSpeedUp = 10;

    //variables privadas
    private Vector3 position;   
    private int blockprevNumber = 0;
    private int champPrevLevel = 1;
    private ChampionScript championScript;
    private float sizecamera;
    private Camera thisCamera;
    private Vector2 camCollOffset;
    //ML-AGENTS
    private GameObject objLimit;
    // Start is called before the first frame update
    void Start()
    {
        onMoveFl = false;
        championScript = Champ.GetComponent<ChampionScript>();
        thisCamera = GetComponent<Camera>();
        Collider2D limitCamera = GetComponent<Collider2D>();
        sizecamera = thisCamera.orthographicSize;
        camCollOffset = new Vector2(-sizecamera*thisCamera.aspect,0);
        limitCamera.offset = camCollOffset;
        if (championScript.IsAgentFl)
        {
            objLimit = Instantiate(new GameObject(), transform.position, Quaternion.identity, transform);
            objLimit.transform.position = transform.position - new Vector3(sizecamera * thisCamera.aspect, 0);
            //Instantiate(objLimit);
            AgentScript agent = championScript.transform.GetComponent<AgentScript>();
            agent.SetCameraLimit(objLimit.transform);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (onMoveFl)
        {
            position = transform.position;
            if (Champ.transform.position.x > position.x-1f)
            {
                //position.x = Champ.transform.position.x-3f;
                position.x += cameraSpeed*10*Time.deltaTime;                
            }
            else
            {
                position.x += cameraSpeed*Time.deltaTime;  
            }
            if(Champ.transform.position.y > -17f)
            {
                position.y = Champ.transform.position.y;
            }            
            transform.position = position;

        }
        //Cada X bloques aumentará la velocidad
        if(startMap.blockNumber% blocksToSpeedUp == 0 && blockprevNumber != startMap.blockNumber)
        {
            cameraSpeed +=0.01f;
            blockprevNumber = startMap.blockNumber;
        }
        //Cada X nivele aumentará la velocidad de la cámara
        if(championScript.Level % 3 == 0 && champPrevLevel != championScript.Level)
        {
            cameraSpeed += 0.01f;
            champPrevLevel = Champ.GetComponent<ChampionScript>().Level;
        }
    }

    public void StopCamera()
    {
        onMoveFl = false;

    }

    public void StartCamera()
    {
        onMoveFl = true;
    }

    public Transform GetCameraLimit()
    {
        if (objLimit != null)
            return objLimit.transform;
        else
        {
            thisCamera = GetComponent<Camera>();           
            sizecamera = thisCamera.orthographicSize;
            

            objLimit = Instantiate(new GameObject(), transform.position, Quaternion.identity, transform);
            objLimit.transform.position = transform.position - new Vector3(sizecamera * thisCamera.aspect, 0);
            return objLimit.transform;
        } 
                
    }
}
