﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoreWeightPotScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        ChampionScript champ = collision.GetComponent<ChampionScript>();

        if (champ != null)
        {
            /*Versión antigua*/
            //champ.AddWeight(0.5f);
            champ.AddWeight(0.25f);
            champ.AddExp(10);
            champ.AddOtherScore(10);
            Destroy(gameObject);
        }

    }

}
