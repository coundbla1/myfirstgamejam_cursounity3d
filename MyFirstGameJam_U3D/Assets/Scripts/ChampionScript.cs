﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/*Script que controla el movimiento del personaje*/
public class ChampionScript : MonoBehaviour
{
    //Variables publicas
    public bool IsAgentFl = false;
    public bool StartGameFl = false;
    public Joystick joystick;
    //Paneles
    public GameObject panelPersonaje;
    //RayCast PJ
    public GameObject RC_Right;
    public GameObject RC_Left;
    //Atributos
    public int lifes = 1;
    public int Level = 1;
    public int Exp = 0;
    public int ExpToNextLevel = 100;
    public int Score = 0;
    public int Health = 1;
    public float Speed;
    public float maxSpeedPerLevel = 2;
    public float JumpForce;
    public float maxJumpForcePerLevel = 500;
    public float DashPower;

    //Habilidades
    public bool H_WallJump = true;
    public bool H_DoubleJump = true;
    public bool H_Dash = false;
    public float H_Dash_Delay = 1f;

    public GameObject DeathPanelObj;

    //Variables privadas
    private Rigidbody2D Rigidbody2D;
    private Animator Animator;
    private AudioSource bgMusic;
    private UpdateScoreScript scoreScript;
    private UpdateStats updateStats;
    private float horizontal;
    private bool grounded;
    private bool jumpFl = false;
    private bool wallJumpFl = false;
    private bool wallCollide;
    private float LastDashUsed;
    //Score
    private int runScore = 0;
    private int otherScore = 0;
    //Base Stats
    private float baseSpeed;
    private float baseJumpForce;
    //RayCasts Champ
    private RaycastHit2D RC0;
    private RaycastHit2D RC1;
    private RaycastHit2D RC2;

    //Agent
    private AgentScript agent;
        //RayCast que avisará si hay un espacio vació delante, un muro o si hay camino por arriba.
    private RaycastHit2D RC_top;//30º desde la cabeza aprox.
    private RaycastHit2D RC_front;// 0º desde el centro
    private RaycastHit2D RC_down;// -45º desde la cabeza.

    private bool posibleJump = false;
    private bool frontObstacle = false;
    private bool topWay = false;



    // Start is called before the first frame update
    void Start()
    {
        //Asignamos a nuestra variable privada el componente RigidBody2D que debe tener nuestro objeto (Champ)
        Rigidbody2D = GetComponent<Rigidbody2D>();
        //Asignamos a nuestra variable privada el animator que debe tener nuestro objeto (Champ)
        Animator = GetComponent<Animator>();

        //Asignamos los rayCast para determinar que alguno de los pies está pisando suelo
        GameObject rayCast = transform.Find("RayCast").gameObject;
        RC_Right = rayCast.transform.Find("RC_right").gameObject;
        RC_Left = rayCast.transform.Find("RC_left").gameObject;
        //Asignamos a la variable privada el audio que tiene el campeon.
        bgMusic = GetComponent<AudioSource>();
        //Reproducimos el audio cuando no es agente.
        if(!IsAgentFl)
            bgMusic.Play();
        
        //TODO: PASAR LA MÜSICA A UN CONTROLADOR EXTERNO. separar de championscript.

        //Cargamos los componentes de los paneles.
        scoreScript = panelPersonaje.GetComponent<UpdateScoreScript>();
        updateStats = panelPersonaje.GetComponent<UpdateStats>();

        //Inicializamos los stats base
        baseSpeed = Speed;
        baseJumpForce = JumpForce;
        updateStats.UpdateSpeed(Speed, baseSpeed + maxSpeedPerLevel);
        updateStats.UpdateJump(JumpForce, baseJumpForce + maxJumpForcePerLevel);
        updateStats.UpdateWeight(Rigidbody2D.mass);
        updateStats.UpdateExp(Exp, ExpToNextLevel);
        updateStats.UpdateLvL(Level);

        //Si es un agente, se carga el agente
        if(IsAgentFl)
            agent = transform.GetComponent<AgentScript>();
        
    }

    // Update is called once per frame
    void Update()
    {
        //Si el flag de empezar el juego no está a true, se retorna para evitar que se mueva el personaje
        if (!StartGameFl)
        {            
            //audio.enabled = false;
            return;
        }
            
        /*Personaje Muerto*/
        //Comprobamos si el personaje tiene 0 de Health, en caso de que sea así se hará return para que no se pueda mover el personaje.
        if (Health <= 0)
        {
            horizontal = 0;
            return;
        }          
        
        /*Movimiento Horizontal*/
        //Asignamos a nuestra variable el Input de dirección. GetAxisRaw devuelve una variable (-1...1) sobre el movimiento en un eje del dis.Entrada (Joystick o teclado)
        //En el caso del teclado, son las flechas. 
        if(!IsAgentFl)
            horizontal = Input.GetAxisRaw("Horizontal");
        if(joystick != null && !IsAgentFl)
        {
            if (joystick.Horizontal != 0)
            {
                if (joystick.Horizontal > 0)
                {
                    horizontal = 1;
                }
                else if (joystick.Horizontal < 0)
                {
                    horizontal = -1;
                }

            }

        }
        //Asignamos a nuestra variable (de animator) Running un booleano (True o false) si nuestro personaje está en movimiento
        Animator.SetBool("Running", horizontal != 0.0f);
        //Colocamos el personaje en la dirección a la que debería apuntar
        if (horizontal < 0.0f) //Izquierda
        {
            transform.localScale = new Vector3(-0.3f, 0.3f, 0.3f);
        }
        else if (horizontal > 0.0f)//derecha
        {
            transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
        }

        /*Movimiento: Salto*/
        //Verificamos si se está pulsando el espacio (teclado)
        if (Input.GetKeyDown(KeyCode.Space)&&!IsAgentFl)
        {
            JumpController();
            Debug.Log("JUMP: " + JumpForce);
        }


        /*Efectos sobre el personaje*/
        //Puntos por distancia
        if (transform.position.x > runScore)
        {
            runScore = (int)transform.position.x;
            Score = runScore + otherScore;
            scoreScript.UpdateScore(Score);
        }
        //Subir Nivel
        if(Exp >= ExpToNextLevel)
        {
            RaiseLevel(1);            
        }
        //Eventos de nivel
        if(Level%10 == 0)
        {
            maxJumpForcePerLevel *= 0.75f;
            maxSpeedPerLevel *= 0.75f;
        }
    }
    /*FIN DE UPDATE()*/
    private void FixedUpdate()
    {
        //Añadimos la velocidad de movimiento del personaje.
        Rigidbody2D.velocity = new Vector2(horizontal * Speed, Rigidbody2D.velocity.y);

        /*Colisión con suelo*/
        //En caso de que el personaje esté tocando el suelo, se pondrá grounded a true, y jumpFl a false
        Debug.DrawRay(transform.position, Vector3.down * 1f, Color.red);
        Debug.DrawRay(RC_Left.transform.position, Vector3.down * 1f, Color.red);
        Debug.DrawRay(RC_Right.transform.position, Vector3.down * 1f, Color.red);
        RC0 = Physics2D.Raycast(transform.position, Vector2.down, 1f);
        RC1 = Physics2D.Raycast(RC_Left.transform.position, Vector2.down, 1f);
        RC2 = Physics2D.Raycast(RC_Right.transform.position, Vector2.down, 1f);
        if (RC0||RC1||RC2)
        {
            grounded = true;
            Animator.SetBool("Jump", false);
        }
        else
        {
            grounded = false;
        }
        /*Colisión con paredes*/
        /*Para pruebas:
        Debug.DrawRay(transform.position, Vector3.right * 0.6f, Color.red);
        Debug.DrawRay(transform.position, Vector3.left * 0.6f, Color.red);
        */
        Debug.DrawRay(transform.position, Vector3.right * 0.6f, Color.red);
        Debug.DrawRay(transform.position, Vector3.left * 0.6f, Color.red);
        if ((Physics2D.Raycast(transform.position, Vector2.right, 0.6f) && horizontal > 0.0f) || (Physics2D.Raycast(transform.position, Vector2.left, 0.6f) && horizontal < 0.0f))
        {
            wallCollide = true;
            if (!grounded)
            {
                wallJumpFl = true;
            }
        }
        else
        {
            wallCollide = false;
        }

        //AGENT
        Debug.DrawRay(transform.position, (transform.right + transform.up).normalized * 5f, Color.green);
        Debug.DrawRay(transform.position, transform.right * 5f, Color.green);
        Debug.DrawRay(transform.position, (transform.right - transform.up).normalized * 5f, Color.green);
        RC_top = Physics2D.Raycast(transform.position, (transform.right + transform.up).normalized, 5f);
        RC_front = Physics2D.Raycast(transform.position, transform.right.normalized, 5f);
        RC_down = Physics2D.Raycast(transform.position, (transform.right - transform.up).normalized, 5f);
        if (RC_top)
        {
            topWay = true;
        }
        if (RC_front){
            frontObstacle = true;
        }
        if (RC_down)
        {
            posibleJump = true;
        }

    }
    /*ML-Agent*/
    public void setHorizontal(float moveX)
    {
        if (moveX > 0)
        {
            horizontal = 1;
        }
        else if (moveX < 0)
        {
            horizontal = -1;
        }
        else horizontal = moveX;
    }
    public void AgentjumpController(int action)
    {
        if (action == 1)
        {
            if (grounded)
            {
                Jump();
                Animator.SetBool("Jump", true);
            }
        }
        if (action == 2)
        {
            if (!grounded && jumpFl)
            {
                DoubleJump();
                Animator.SetBool("Jump", true);
            }
        }

    }

    public void SetAgentReward(int reward)
    {
        if (!IsAgentFl)
        {
            Debug.Log("ERROR, no es un agente");
            return;
        }
        if(agent == null)
        {
            agent = transform.GetComponent<AgentScript>();
        }
        agent.SetReward(reward);
    }

    public bool getWallCollide()
    {
        return wallCollide;
    }

    public bool getGrounded()
    {
        return grounded;
    }

    public bool getPosibleJump()
    {
        return posibleJump;
    }
    public float getPosibleJumpDistance()
    {
        return RC_down.distance;
    }
    public bool getFrontObstacle()
    {
        return frontObstacle;
    }
    public float getObstacleDistance()
    {
        return RC_front.distance;
    }
    public bool getTopWay()
    {
        return topWay;
    }
    public float getTopWayDistance()
    {
        return RC_top.distance;
    }
    
    /**FUNCIONES DEL PERSONAJE*/
    /*CONTOL DEL JUEGO*/
    public void StartGame()
    {
        StartGameFl = true;
        
        //audio.enabled = true;
    }
    public void StopGame()
    {
        StartGameFl = false;
        Camera camera = Camera.main;
        CameraScript camScript = camera.GetComponent<CameraScript>();
        camScript.StopCamera();
    }
    public void RestartGame()
    {
        //PlayerPrefs.SetInt("ISAGENTFINISHED", 1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //agent.EndEpisode();
    }
    public void StartMenu()
    {
        SceneManager.LoadScene(0);
    }

    /**MOVIMIENTO DEL CAMPEON*/
    public void JumpController()
    {
        //Si al presionar "espacio", el personaje está en el suelo, realizará un salto, y activará el flag para el siguiente salto
         if (grounded)
        {
            Jump();
            Animator.SetBool("Jump", true);

        }
        else if (jumpFl)
        {
            DoubleJump();
            Animator.SetBool("Jump", true);

        }
        else if (wallJumpFl)
        {
            Animator.SetBool("Jump", true);
            Jump();
            wallJumpFl = false;
        }
    }
    
    /*Función para saltar: añade fuerza variable hacia arriba al campeón*/
    private void Jump()
    {
        Rigidbody2D.AddForce(Vector2.up * JumpForce);//Probar con modo impulse        
        jumpFl = true;
    }
    /*Función que realiza el segundo salto. Este salto tendrá menos potencia que el primero*/
    private void DoubleJump()
    {
        Rigidbody2D.AddForce(Vector2.up * JumpForce/100, ForceMode2D.Impulse);
        //Rigidbody2D.AddForce(Vector2.up * JumpForce * 0.75f);
        jumpFl = false;
    }
    private void Dash()
    {
        if(LastDashUsed + 5 <= Time.fixedTime)
        {
            return;
        }

        if(horizontal < 0.0f)
        {
            Rigidbody2D.AddForce(Vector2.right * DashPower);

        }
        
        if(horizontal > 0.0f)
        {
            Rigidbody2D.AddForce(Vector2.left * DashPower);
        }
        LastDashUsed = Time.fixedTime;
        
    }
    /*EFECTOS SOBRE EL CAMPEON*/
    public void Hit()
    {
        
        
        Health--;

        if(Health <= 0)
        {
            Kill();
        }
    }
    public void Kill()
    {
        StopGame();
        Animator.SetBool("Dead", true);
        //Ocultamos el panel del personaje
        panelPersonaje.SetActive(false);
        //Calculamos el score final y level y actualizamos el panel.
        DeadPanelScript deadPanelScript = DeathPanelObj.GetComponent<DeadPanelScript>();
        deadPanelScript.UpdateScoreInfo(Score);
        deadPanelScript.UpdateLevelInfo(Level);
        //Invocamos el canvas de fin de partida
        if (!IsAgentFl)
            DeathPanelObj.SetActive(true);
        else
            SetAgentReward(0);
    }

    public void AddWeight(float peso)
    {
        Rigidbody2D.mass += peso;
        updateStats.UpdateWeight(Rigidbody2D.mass);
    }

    public void EditSpeed(float spd)
    {
        float maxSpd = baseSpeed + maxSpeedPerLevel;
        if(Speed < maxSpd)
        {
            Speed += spd;
            //Reducimos a 2 decimales
            if(Speed%1 > 2)
            {
                Speed = Mathf.Round(Speed * 100f) / 100f;
            }            
            if (Speed >= maxSpd)
            {
                Speed = maxSpd;
            }
            updateStats.UpdateSpeed(Speed, maxSpd);
        }
    }
    public void EditJumpForce(float jump)
    {
        float maxjump = baseJumpForce + maxJumpForcePerLevel;
        if (JumpForce < maxjump)
        {
            JumpForce += jump;
            if (JumpForce > maxjump)
            {
                JumpForce = maxjump;
            }
            updateStats.UpdateJump(JumpForce, baseJumpForce + maxJumpForcePerLevel);
        }
    }
    public void AddExp(int exp)
    {
        Exp += exp;
        updateStats.UpdateExp(Exp, ExpToNextLevel);
    }
    private void CalculateNextLevelExp()
    {
        ExpToNextLevel *= 2;
    }
    //Función para subir el nivel del personaje. Aumenta 200 puntos de score por nivel subido. (Poner como variable publica???)
    public void RaiseLevel(int numLvL)
    {
        int scr;
        Level += numLvL;
        scr = numLvL * 200;
        CalculateNextLevelExp();
        EditSpeed(0.2f);
        EditJumpForce(50f);
        AddOtherScore(scr);
        UpdateBaseStatsOnLevelRaise(numLvL);
        scoreScript.UpdateScore(Score);
        updateStats.UpdateLvL(Level);
        updateStats.UpdateExp(Exp, ExpToNextLevel);
    }
    //Función que añade score de recolección de objetos
    public void AddOtherScore(int scr)
    {
        otherScore += scr;
        Score = otherScore+runScore;
        scoreScript.UpdateScore(Score);
        if (IsAgentFl)
        {
            SetAgentReward(scr/10);
        }
    }
    //Función que actualiza los statsbase (speed, jump) de acuerdo al nº niveles subidos
    private void UpdateBaseStatsOnLevelRaise(int numLvL)
    {        
        baseSpeed += maxSpeedPerLevel * numLvL;
        baseJumpForce += maxJumpForcePerLevel * numLvL;
        float maxSpd = baseSpeed + maxSpeedPerLevel;
        float maxjmp = baseJumpForce + maxJumpForcePerLevel;
        updateStats.UpdateSpeed(Speed, maxSpd);
        updateStats.UpdateJump(JumpForce, maxjmp);
    }
    /**COLISIONES*/
    //Función que comprueba que no seas aplastado por la camara y una pared al colisionar con la camara.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        CameraScript camScript = collision.collider.GetComponent<CameraScript>();        
        if(camScript != null && !IsAgentFl)
        {
            Kill();
        }
    }
    //FUnción que comprueba que no seas aplastado por la camara y una pared al mantenerte tocando la camara.
    private void OnCollisionStay2D(Collision2D collision)
    {
        CameraScript camScript = collision.collider.GetComponent<CameraScript>();        
        if (camScript != null && wallCollide && !IsAgentFl)
        {
            Kill();
        }
    }


}
