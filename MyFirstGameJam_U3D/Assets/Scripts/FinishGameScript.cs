﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishGameScript : MonoBehaviour
{
    public GameObject EndPanel;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        ChampionScript champ = collision.GetComponent<ChampionScript>();

        if (champ != null)
        {
            champ.StopGame();           
            EndPanel.SetActive(true);
        }
    }

}
