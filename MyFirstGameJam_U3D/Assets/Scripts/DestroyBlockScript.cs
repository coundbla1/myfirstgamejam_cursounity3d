﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBlockScript : MonoBehaviour
{    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        CameraScript camScript = collision.GetComponent<CameraScript>();
        
        if (camScript != null)
        {
            GameObject parentObj = transform.parent.gameObject;
            Destroy(parentObj);
        }
    }
}
