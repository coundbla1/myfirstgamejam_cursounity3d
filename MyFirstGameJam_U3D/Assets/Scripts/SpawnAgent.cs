using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAgent : MonoBehaviour
{
    public GameObject agentPref;

    private GameObject actualAgent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void spawnAgent()
    {
        if (actualAgent)
        {
            Destroy(actualAgent);
        }
        actualAgent = Instantiate(agentPref, transform);
    }
}
