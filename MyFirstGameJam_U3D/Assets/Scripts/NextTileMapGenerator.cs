﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
public class NextTileMapGenerator : MonoBehaviour
{
    public GameObject[] mapBlocks;


    private int randomNumber;
    private float randomPositionY;
    private float randomPositionX;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        ChampionScript champ = collision.GetComponent<ChampionScript>();

        if (champ != null)
        {            
            randomNumber = Random.Range(0, mapBlocks.Length);
            randomPositionY = Random.Range(1f, 5f);
            if(randomNumber == 0||randomNumber == 3)
            {
                randomPositionX = Random.Range(4.6f, 6.1f);
                if(randomNumber == 0)
                {
                    randomPositionY = Random.Range(-1f, 2f);
                }
            }
            else
            {
                randomPositionX = Random.Range(6f, 7.5f);
            }
            
            GameObject mapGeneratorController = GameObject.Find("MapGeneratorController");
            StartMap startMap = mapGeneratorController.GetComponent<StartMap>();
            Debug.Log("LastTilePositionX: " + startMap.lastTilePositionX);
            Vector3 blockPosition = new Vector3(startMap.lastTilePositionX + (startMap.tilemapLenght*0.5f)+randomPositionX, randomPositionY, 0);
            GameObject newBlock = Instantiate(mapBlocks[randomNumber], blockPosition, Quaternion.identity);
            startMap.setLastBlockInserted(newBlock);
            GameObject TileMapObj = newBlock.transform.Find("Tilemap").gameObject;
            Tilemap tilemap = TileMapObj.GetComponent<Tilemap>();
            Bounds tilemapBounds = tilemap.localBounds;
            startMap.UpdateLastTilePositionX(startMap.lastTilePositionX + tilemapBounds.max.x+randomPositionX*2);
            startMap.UpdateBlockNumber();
            Debug.Log("NEW_LastTilePositionX: " + startMap.lastTilePositionX);
            Destroy(gameObject);
            //ML_AGENTS
            if (champ.IsAgentFl)
            {
                AgentScript agent = champ.transform.GetComponent<AgentScript>();
                Transform objetive = newBlock.transform.Find("SpawnNextTileMap");
                agent.setObjetive(objetive);
            }
        }
    }
}
