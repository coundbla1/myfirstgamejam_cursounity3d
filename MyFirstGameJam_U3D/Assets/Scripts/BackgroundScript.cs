﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
/*Script que se encarga del movimiento del background.*/
public class BackgroundScript : MonoBehaviour
{
   //Variables publicas
    public GameObject Champ;
    public bool MoveFl = false;
    public float varSpeed;
    public Bounds bounds;
    public bool isFinished = false;
    public float offset;

    //Variables privadas    
    private Vector2 size;
    private float limitToCenterXlenght;
    
    private SpriteRenderer spriteRenderer;
    private float lastX;
    // Update is called once per frame
    private void Start()
    {        
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        size = spriteRenderer.size;
        limitToCenterXlenght = size.x / 2;
        bounds = spriteRenderer.bounds;
        offset = transform.position.x - Champ.transform.position.x;
        //Comprobamos que el offset sea el global
        GameObject controller = GameObject.Find("MapGeneratorController");
        StartMap startMap = controller.GetComponent<StartMap>();
        if(startMap.bgNumber == 1)
        {
            startMap.UpdateChampBGOffset(offset);
        }
        if (startMap.champBGOffset != 0 && (startMap.champBGOffset != offset))
        {
            offset = startMap.champBGOffset;
        }        
        //Asignamos a lastX la posicion central actual.
        lastX = transform.position.x;
        //Añadimos el champ en escena
        Champ = GameObject.FindGameObjectWithTag("Player");
        bool startFl = Champ.GetComponent<ChampionScript>().StartGameFl;
        Debug.Log("CREAR BG - Ha recuperado champ -> " + Champ.name + "\nVariable Start -> " + startFl);
        if (startFl)
        {
            MoveFl = true;
        }
        varSpeed = 0.1f;

        //Añadimos el confiner a la camara
        CinemachineVirtualCamera virtualCamera = CinemachineVirtualCamera.FindObjectOfType<CinemachineVirtualCamera>();
        CinemachineConfiner confiner = virtualCamera.GetComponent<CinemachineConfiner>();
        confiner.m_BoundingShape2D = GameObject.Find("Confiner").GetComponent<Collider2D>();
    }
    void Update()
    {
        if (MoveFl)
        {            
            
            Vector3 position = transform.position;

            position = new Vector3(Champ.transform.position.x + offset, transform.position.y, transform.position.z);            
            /*if (Champ.transform.position.y <= -20)
            {
                position.y = transform.position.y - 0.5f;
            }
            if (!isFinished && (lastX < position.x) && (offset > (transform.position.x - Champ.transform.position.x)))
            {
                transform.position = position;
                lastX = position.x;
                offset -= varSpeed;
            }*/
            if(transform.position.x + bounds.extents.x*0.75 <= Champ.transform.position.x)
            {
                BGStop();
                 isFinished = true;
                StartMap startMap = GameObject.Find("MapGeneratorController").GetComponent<StartMap>();
                startMap.UpdateIsBGFinished(isFinished);
            }           
        }
        else if(Champ.transform.position.x >= transform.position.x  && Champ.GetComponent<ChampionScript>().StartGameFl && !isFinished)
        {
            BGStart();
        }
    }
    public void BGStart()
    {        
        MoveFl = true;        
    }
    public void BGStop()
    {
        MoveFl = false;
    }
    public void InitPosition(Vector3 vector)
    {
        transform.position = vector;
    }
}
