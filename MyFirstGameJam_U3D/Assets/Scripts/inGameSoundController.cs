﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class inGameSoundController : MonoBehaviour
{
    public bool MusicOn;
    public GameObject Champ;
    public Sprite MusicOnImg;
    public Sprite MusicOffImg;
    public void StartStopMusic() 
    {
        AudioSource bgmusic = Champ.GetComponent<AudioSource>();
        GameObject musicButton = GameObject.Find("MusicOnOff");
        Image buttonimg = musicButton.GetComponent<Image>();
        if (MusicOn)
        {
            bgmusic.mute = true;            
            buttonimg.sprite = MusicOffImg;
            MusicOn = false;
        }
        else
        {
            bgmusic.mute = false;
            buttonimg.sprite = MusicOnImg;
            MusicOn = true;
        }
    }
}
