﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowPotionScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        ChampionScript champ = collision.GetComponent<ChampionScript>();

        if (champ != null)
        {
            //champ.EditSpeed(-0.5f);
            champ.EditSpeed(-0.3f);
            champ.AddExp(10);
            champ.AddOtherScore(10);
            Destroy(gameObject);
        }
    }

}
