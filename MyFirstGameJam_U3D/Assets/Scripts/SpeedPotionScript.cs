﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedPotionScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        ChampionScript champ = collision.GetComponent<ChampionScript>();

        if (champ != null)
        {
            /*Versión antigua*/
            //champ.EditSpeed(1f);
            champ.EditSpeed(0.2f);
            champ.AddExp(10);
            champ.AddOtherScore(20);
            Destroy(gameObject);            
        }
    }
}
