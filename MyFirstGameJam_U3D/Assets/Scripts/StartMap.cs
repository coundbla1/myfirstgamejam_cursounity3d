﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class StartMap : MonoBehaviour
{
    //Variables publicas
    public GameObject[] arrayBlocks;
    public GameObject[] arrayBG;
    public ChampionScript champ;
    public float lastTilePositionX;
    public float lastBGPositionX;
    public float tilemapLenght;
    public int blockNumber=0;
    public int bgNumber = 0;
    public bool isLastBGFinished = false;
    public GameObject lastBGobj;
    public float champBGOffset;
    public GameObject lastBlockInserted;

    //Variables privadas
    private int bgArrayNumber = 0;

    // Start is called before the first frame update
    void Start()
    {        
        Vector3 InitPosition = new Vector3(champ.transform.position.x, champ.transform.position.y - 2, champ.transform.position.z);
        GameObject FirstBlock = Instantiate(arrayBlocks[0], InitPosition, Quaternion.identity);
        lastBlockInserted = FirstBlock;
        GameObject TileMapObj = FirstBlock.transform.Find("Tilemap").gameObject;
        Tilemap tilemap = TileMapObj.GetComponent<Tilemap>();
        Bounds tilemapBounds = tilemap.localBounds;
        lastTilePositionX = tilemapBounds.max.x;
        tilemapLenght = tilemapBounds.extents.x;
        blockNumber = 1;
        lastBGobj = Instantiate(arrayBG[bgArrayNumber]);        
        bgNumber++;
        bgArrayNumber++;
        lastBGPositionX = lastBGobj.transform.position.x;
        Debug.Log(lastTilePositionX);
        //ML-AGENT
        if(champ.IsAgentFl)
        {
            AgentScript agent = champ.transform.GetComponent<AgentScript>();
            Transform objetive = lastBlockInserted.transform.Find("SpawnNextTileMap");
            agent.setObjetive(objetive);
        }
    }
    private void Update()
    {        
        if (isLastBGFinished)
        {
            UpdateIsBGFinished(false);            
            BackgroundScript backgroundScript = lastBGobj.GetComponent<BackgroundScript>();
            backgroundScript.BGStop();
            Bounds bounds = backgroundScript.bounds;
            Vector3 InitPosition = new Vector3(bounds.max.x+bounds.extents.x, lastBGobj.transform.position.y, 0);
            lastBGobj = Instantiate(arrayBG[bgArrayNumber], InitPosition,Quaternion.identity);
            lastBGobj.transform.position = InitPosition;
            bgNumber++;
            bgArrayNumber++;
            if (bgArrayNumber >= arrayBG.Length)
            {
                bgArrayNumber = 0;
            }
            lastBGPositionX = lastBGobj.transform.position.x;
        }
    }
    public void UpdateLastTilePositionX(float newX)
    {        
        lastTilePositionX = newX;
        blockNumber++;        
    }
    public void UpdateLastBG(GameObject bg)
    {
        lastBGobj = bg;
        bgNumber++;
    }
    public void UpdateIsBGFinished(bool isfinish)
    {
        isLastBGFinished = isfinish;
    }
    public void UpdateLastBGPositionX(float newX)
    {
        lastBGPositionX = newX;
    }
    public void UpdateBlockNumber()
    {
        blockNumber++;
    }
    public void UpdateChampBGOffset(float offset)
    {
        champBGOffset = offset;
    }
    public void StartBG()
    {
        Debug.Log("START BG!");
        BackgroundScript backgroundScript = lastBGobj.GetComponent<BackgroundScript>();
        backgroundScript.BGStart();

    }
    public void setChamp(ChampionScript Champion)
    {
        champ = Champion;
    }

    public void setLastBlockInserted(GameObject obj)
    {
        lastBlockInserted = obj;
    }
        

}
