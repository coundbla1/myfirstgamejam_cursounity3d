﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
public class PoisonSpawnScript : MonoBehaviour
{
    public GameObject[] PoisonObjects;


    private int randomNumber;
    // Start is called before the first frame update
    void Start()
    {
        Tilemap tilemap = GetComponentInParent<Tilemap>();
        randomNumber = Random.Range(0, PoisonObjects.Length);
        float envObjHeigh = PoisonObjects[randomNumber].transform.localScale.y;
        float positionY = transform.position.y + envObjHeigh / 2;
        Vector3 initPosition = GetInitPosition(positionY);
        GameObject objeto = Instantiate(PoisonObjects[randomNumber], initPosition, Quaternion.identity);
        GameObject block = transform.parent.gameObject;
        BlockController bc = block.GetComponent<BlockController>();
        bc.UpdateArrayObjects(objeto);

    }

    private Vector3 GetInitPosition(float positionY)
    {
        float modPositionY = 0;
        //Condicionales para alterar la posición Y del objecto al instanciarlo.
        if (PoisonObjects[randomNumber].name == "MoreWeightPotion")
        {
            modPositionY = 0.4f;
        }
        if (PoisonObjects[randomNumber].name == "SlowPotion")
        {
            modPositionY = 0.4f;
        }
        positionY += modPositionY;
        Vector3 initPosition = new Vector3(transform.position.x, positionY, 0);

        return initPosition;
    }
}
