﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PotionSpawnPointScript : MonoBehaviour
{
    public GameObject[] PotionObjects;


    private int randomNumber;
    // Start is called before the first frame update
    void Start()
    {
        Tilemap tilemap = GetComponentInParent<Tilemap>();
        randomNumber = Random.Range(0, PotionObjects.Length);
        float envObjHeigh = PotionObjects[randomNumber].transform.localScale.y;
        float positionY = transform.position.y + envObjHeigh / 2;
        Vector3 initPosition = GetInitPosition(positionY);
        GameObject objeto = Instantiate(PotionObjects[randomNumber], initPosition, Quaternion.identity);
        GameObject block = transform.parent.gameObject;
        BlockController bc = block.GetComponent<BlockController>();
        bc.UpdateArrayObjects(objeto);

    }

    private Vector3 GetInitPosition(float positionY)
    {
        float modPositionY = 0;
        //Condicionales para alterar la posición Y del objecto al instanciarlo.
        if (PotionObjects[randomNumber].name == "JumpPotion")
        {
            modPositionY = 0.3f;
        }
        if (PotionObjects[randomNumber].name == "SpeedPotion")
        {
            modPositionY = 0.3f;
        }        
        positionY += modPositionY;
        Vector3 initPosition = new Vector3(transform.position.x, positionY, transform.position.y);

        return initPosition;
    }
}
