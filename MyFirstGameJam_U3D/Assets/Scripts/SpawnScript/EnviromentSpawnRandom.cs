﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class EnviromentSpawnRandom : MonoBehaviour
{
    public GameObject[] EnviromentObjects;


    private int randomNumber;
    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(CoStart());
        randomNumber = Random.Range(0, EnviromentObjects.Length);
        float envObjHeigh = EnviromentObjects[randomNumber].transform.localScale.y;
        float positionY = transform.position.y + envObjHeigh / 2;
        Vector3 initPosition = GetInitPosition(positionY);        
        GameObject objeto = Instantiate(EnviromentObjects[randomNumber], initPosition, Quaternion.identity);
        GameObject block = transform.parent.gameObject;
        BlockController bc = block.GetComponent<BlockController>();
        bc.UpdateArrayObjects(objeto);
    }
    IEnumerator CoStart()
    {
        yield return new WaitForSeconds(1);

        /*GameObject parentObj = transform.parent.gameObject;
        Debug.Log(parentObj.name);
        GameObject TileMap = parentObj.transform.Find("Tilemap").gameObject;
        Debug.Log(TileMap.name);
        Tilemap tilemap = TileMap.GetComponent<Tilemap>();*/
        //Tilemap tilemap = GetComponentInParent<Tilemap>();
        //Debug.Log(tilemap.localBounds);
        randomNumber = Random.Range(0, EnviromentObjects.Length);
        float envObjHeigh = EnviromentObjects[randomNumber].transform.localScale.y;
        float positionY = transform.position.y + envObjHeigh / 2;
        Vector3 initPosition = GetInitPosition(positionY);
        Debug.Log("DEBERÏA APARECER UN CARTEL O ARBOL");
        Instantiate(EnviromentObjects[randomNumber], initPosition, Quaternion.identity);
        yield return null;
    }
    private Vector3 GetInitPosition(float positionY)
    {
        float modPositionY = 0;
        if(EnviromentObjects[randomNumber].name == "Tree")
        {            
            modPositionY = 3f;
        }

        positionY += modPositionY;
        Vector3 initPosition = new Vector3(transform.position.x, positionY, transform.position.y);

        return initPosition;
    }
}
