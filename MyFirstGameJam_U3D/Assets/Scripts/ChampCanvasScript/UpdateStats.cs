﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UpdateStats : MonoBehaviour
{
    public GameObject LvLInfo;
    //public GameObject MinLvlInfo;
    public GameObject ExpInfo;
    public GameObject WeightInfo;    
    public GameObject SpeedInfo;
    public GameObject JumpInfo;

    public void UpdateLvL(int LvL)
    {
        TextMeshProUGUI textMeshPro = LvLInfo.GetComponent<TextMeshProUGUI>();
        //TextMeshProUGUI textMeshPro2 = MinLvlInfo.GetComponent<TextMeshProUGUI>();
        textMeshPro.text = LvL.ToString();
        //textMeshPro2.text = LvL.ToString();
    }
    public void UpdateExp(int Exp, int ExpToNextLvL)
    {
        TextMeshProUGUI textMeshPro = ExpInfo.GetComponent<TextMeshProUGUI>();
        textMeshPro.text = $"{Exp}/{ExpToNextLvL}";
    }

    public void UpdateWeight(float Weight)
    {
        TextMeshProUGUI textMeshPro = WeightInfo.GetComponent<TextMeshProUGUI>();
        textMeshPro.text = Weight.ToString();
    }

    public void UpdateSpeed(float Speed, float MaxSpeed)
    {
        TextMeshProUGUI textMeshPro = SpeedInfo.GetComponent<TextMeshProUGUI>();
        textMeshPro.text = $"{Speed}/{MaxSpeed}";
    }
    public void UpdateJump(float JumpForce, float MaxJumpForce)
    {
        float jmp = JumpForce / 500f;
        float maxjmp = MaxJumpForce / 500f;
        TextMeshProUGUI textMeshPro = JumpInfo.GetComponent<TextMeshProUGUI>();
        textMeshPro.text = $"{jmp:F2}/{maxjmp:F2}";
    }

}
