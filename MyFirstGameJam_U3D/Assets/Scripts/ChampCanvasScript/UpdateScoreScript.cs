﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using TMPro;//Paquete para textMeshPro

public class UpdateScoreScript : MonoBehaviour
{
    // Update is called once per frame
    public GameObject ScoreNumber;

    public void UpdateScore(int score)
    {        
        TextMeshProUGUI textMeshPro = ScoreNumber.GetComponent<TextMeshProUGUI>();
        textMeshPro.text = score.ToString();
    }
}
