﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DeadPanelScript : MonoBehaviour
{
    public GameObject MaxScoreInfo;
    public GameObject MaxLevelInfo;
    public GameObject ScoreInfo;
    public GameObject LevelInfo;

    private int maxScr;
    public void UpdateMaxScoreInfo(int MaxScore)
    {
        PlayerPrefs.SetInt("MaxScore", MaxScore);
        TextMeshProUGUI textMeshPro = MaxScoreInfo.GetComponent<TextMeshProUGUI>();
        textMeshPro.text = MaxScore.ToString();
    }
    public void UpdateMaxLevelInfo(int MaxLevel)
    {
        PlayerPrefs.SetInt("MaxLevel", MaxLevel);
        TextMeshProUGUI textMeshPro = MaxLevelInfo.GetComponent<TextMeshProUGUI>();
        textMeshPro.text = MaxLevel.ToString();
    }

    public void UpdateScoreInfo(int Score)
    {
        int MaxScore = PlayerPrefs.GetInt("MaxScore",0);
        if(MaxScore == 0 )
        {
            UpdateMaxScoreInfo(Score);
        }
        else
        {
            if (MaxScore < Score)
            {
                UpdateMaxScoreInfo(Score);
            }
            else
            {
                UpdateMaxScoreInfo(MaxScore);
            }
        }
        TextMeshProUGUI textMeshPro = ScoreInfo.GetComponent<TextMeshProUGUI>();
        textMeshPro.text = Score.ToString();
    }

    public void UpdateLevelInfo(int Level)
    {
        int MaxLevel = PlayerPrefs.GetInt("MaxLevel", 0);
        if (MaxLevel == 0)
        {
            UpdateMaxLevelInfo(Level);
        }
        else
        {
            if (Level>MaxLevel)
            {
                UpdateMaxLevelInfo(Level);
            }
            else
            {
                UpdateMaxLevelInfo(MaxLevel);
            }
        }

        TextMeshProUGUI textMeshPro = LevelInfo.GetComponent<TextMeshProUGUI>();
        textMeshPro.text = Level.ToString();
    }
}
